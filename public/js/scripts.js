$(document).ready(function() {  
	// Mostrar ventana ley de cookies.
    $('#cookieModal').modal('show');

    // $("#modal-dialogs").load('../common/modal-dialogs.html');
    $("#btn-add-to-cart").submit(function(e) {
        // alert("submit intercepted" + $('#id_product').val());
        $('#productDetailModal').modal('show');
        e.preventDefault(e);
    });

    // Borrar modal ley de cookies por 365 días.
    /* 
    bloqueRGPD = document.getElementById("cookieModal");
    if(detectCookie("rgpdOK")){
        if (getCookie("rgpdOK")==1){eliminarBloqueRGPD();}
    }else{
        document.getElementById("btn-accept-cookies").addEventListener("click",function(){
        	// eliminarBloqueRGPD
        	bloqueRGPD.parentNode.removeChild(bloqueRGPD);
        	setCookie("rgpdOK",1,365);
        })        
    }
    */
    
    // https://codepen.io/MOgorodnik/pen/AXwQmY
    // Add scrollspy to <body>
    $('body').scrollspy({target: ".menuproductes", offset: 50});  
    // Add smooth scrolling on all links inside the navbar
    $("#basicExampleNav1 a").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function(){
    
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
        }  // End if
    });

});
